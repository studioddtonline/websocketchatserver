/**
 * spring.sql.init.separatorを設定し
 * デリミタが^;に変更してある
 */

/**
 * チャット用テーブル
 */
CREATE TABLE IF NOT EXISTS chat_messages (
    id CHAR(26) PRIMARY KEY,
    sender CHAR(36),
    name TEXT(4000),
    content TEXT(30000),
	created_at TIMESTAMP NOT NULL
)^;
CREATE TABLE IF NOT EXISTS chat_users (
    session_id CHAR(36) PRIMARY KEY,
    name TEXT(4000)
)^;

/**
 * Spring Session用のテーブル
 */

 CREATE TABLE IF NOT EXISTS SPRING_SESSION (
	PRIMARY_ID CHAR(36) NOT NULL,
	SESSION_ID CHAR(36) NOT NULL,
	CREATION_TIME BIGINT NOT NULL,
	LAST_ACCESS_TIME BIGINT NOT NULL,
	MAX_INACTIVE_INTERVAL INT NOT NULL,
	EXPIRY_TIME BIGINT NOT NULL,
	PRINCIPAL_NAME VARCHAR(100),
	CONSTRAINT SPRING_SESSION_PK PRIMARY KEY (PRIMARY_ID)
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC^;

/**
 * 重複したINDEX生成を阻害するストアドプロシージャ
 */

DROP PROCEDURE IF EXISTS `CreateIndex` ^;
CREATE PROCEDURE `CreateIndex`
(
    given_database VARCHAR(64),
    given_table    VARCHAR(64),
    given_unique   VARCHAR(64),
    given_index    VARCHAR(64),
    given_columns  VARCHAR(64)
)
BEGIN

    DECLARE hasIndex INTEGER;

    SELECT COUNT(1) INTO hasIndex
    FROM INFORMATION_SCHEMA.STATISTICS
    WHERE table_schema = given_database
    AND   table_name   = given_table
    AND   index_name   = given_index;

    IF hasIndex = 0 THEN
        SET @sqlstmt = CONCAT('CREATE ', given_unique, ' INDEX ', given_index, ' ON ',
        given_database,'.',given_table,' (',given_columns,')');
        PREPARE st FROM @sqlstmt;
        EXECUTE st;
        DEALLOCATE PREPARE st;
    ELSE
        SELECT NULL;   
    END IF;
END ^;

/**
 * INDEX生成
 */

CALL CreateIndex('websocket_chat', 'SPRING_SESSION', 'UNIQUE', 'SPRING_SESSION_IX1', 'SESSION_ID')^;
CALL CreateIndex('websocket_chat', 'SPRING_SESSION', '', 'SPRING_SESSION_IX2', 'EXPIRY_TIME')^;
CALL CreateIndex('websocket_chat', 'SPRING_SESSION', '', 'SPRING_SESSION_IX3', 'PRINCIPAL_NAME')^;

/**
 * Spring SessionのAttribute用のテーブル
 */

CREATE TABLE IF NOT EXISTS SPRING_SESSION_ATTRIBUTES (
	SESSION_PRIMARY_ID CHAR(36) NOT NULL,
	ATTRIBUTE_NAME VARCHAR(200) NOT NULL,
	ATTRIBUTE_BYTES BLOB NOT NULL,
	CONSTRAINT SPRING_SESSION_ATTRIBUTES_PK PRIMARY KEY (SESSION_PRIMARY_ID, ATTRIBUTE_NAME),
	CONSTRAINT SPRING_SESSION_ATTRIBUTES_FK FOREIGN KEY (SESSION_PRIMARY_ID) REFERENCES SPRING_SESSION(PRIMARY_ID) ON DELETE CASCADE
) ENGINE=InnoDB ROW_FORMAT=DYNAMIC^;