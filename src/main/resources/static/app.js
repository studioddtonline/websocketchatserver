let stompClient = null;

function setConnected(connected) {
  $("#connect").prop("disabled", connected);
  $("#disconnect").prop("disabled", !connected);
  $("#send").prop("disabled", !connected);
  $("#messages").empty();
}

function connect() {
  stompClient = Stomp.client(
    `ws://${new URL(location.href).host}/stompOverWebsocket`
  );
  stompClient.connect(
    {},
    function (frame) {
      setConnected(true);
      console.log("Connected: " + frame);
      ["/topic/message", "/topic/clean", "/user/queue/init"].forEach(
        (endpoint) => {
          stompClient.subscribe(endpoint, commandRunner);
        }
      );
      stompClient.send("/app/init", {}, "{}");
    },
    function (error) {
      setConnected(false);
      console.log("Disconnected: " + error);
    }
  );
}

function commandRunner(rowResponse) {
  const parsedResponse = JSON.parse(rowResponse.body);
  if (!Array.isArray(parsedResponse)) {
    rowResponse.body = `[${rowResponse.body}]`;
    return commandRunner(rowResponse);
  }
  parsedResponse.forEach((context) => {
    switch (context.command) {
      case "APPEND":
        const messages = context.messages
          .map((m) => buildMessage(m.id, m.name, m.content, m.sender))
          .join();
        showMessages(messages);
        break;
      case "CLEAR":
        $("#messages").empty();
        break;
      case "DELETE":
        context.messages.map((m) => removeMessage(m.id));
        break;
      case "SET":
        $("#name").val(context?.entity?.name || "");
        $("#sender").val(context?.entity?.sessionId || "");
        break;
      case "INFO":
      case "ERROR":
      default:
        console.log(context);
        break;
    }
  });
}

function disconnect() {
  if (stompClient !== null) {
    stompClient.disconnect();
  }
  setConnected(false);
  console.log("Disconnected");
}

function validateString(str = "", maxLength = 1000) {
  return str.trim().substring(0, Math.max(maxLength, 0));
}

function sendMessage() {
  const name = validateString($("#name").val(), 2000);
  const content = validateString($("#content").val(), 15000);
  const sender = validateString($("#sender").val(), 36);
  if (name == "" || content == "") {
    return;
  }
  stompClient.send(
    "/app/message",
    {},
    JSON.stringify({ name, content, sender })
  );
  $("#content").val("");
  $("*:focus").blur();
}

function buildMessage(id, name, content, sender) {
  const isSelf = sender == $("#sender").val();
  return `<tr id='${id}' class='my-2 ${
    isSelf ? "mr-5 flex-column" : "ml-5 flex-column-reverse"
  } d-flex'>
  <td class="name text-light text-break small font-weight-bold border-dark border-right border-left px-2 py-1 ${
    isSelf
      ? "border-top rounded-top bg-primary"
      : "border-bottom rounded-bottom bg-secondary text-right"
  }">${name}</td>
  <td class="message text-break border-dark border-right border-left p-2 ${
    isSelf
      ? "border-bottom rounded-bottom"
      : "border-top rounded-top text-right"
  }">${content}</td>
  </tr>`;
}

function showMessages(messages) {
  $(messages)
    .stop()
    .appendTo("#messages")
    .hide()
    .fadeIn(function () {
      const mEl = document.getElementById("messages");
      const scrollAmount =
        mEl.scrollHeight - $(mEl.lastChild).outerHeight(true);
      $(mEl).stop().animate({ scrollTop: scrollAmount }, 1000);
    });
}

function removeMessage(id) {
  $("#" + id)
    .stop()
    .animate({ opacity: 0, height: 0 }, "400", "swing", function () {
      this.remove();
    });
}

function observeResize() {
  const handleResizeObserver = (entries) => {
    const el = $("#messages");
    const winHeight = window.innerHeight;
    const messagesTop = el.offset().top + 1;
    el.height(winHeight - messagesTop);
    if (el[0].scrollHeight > el.height()) {
      el.css("overflow-y", "scroll");
    } else {
      el.css("overflow-y", "hidden");
    }
  };
  const ro = new ResizeObserver(handleResizeObserver);
  ro.observe(document.querySelector("#main-content"));
  window.addEventListener("resize", handleResizeObserver);
}

function observeMessages() {
  const handleMessagesChildObserver = (records) => {
    records.forEach((record) => {
      if (record.target.children.length > 0) {
        document.getElementById("table-head").textContent = "messages";
      } else {
        document.getElementById("table-head").textContent = "no messages";
      }
      window.dispatchEvent(new Event("resize"));
    });
  };
  const mo = new MutationObserver(handleMessagesChildObserver);
  mo.observe(document.querySelector("#messages"), { childList: true });
}

$(function () {
  $("form").on("submit", function (e) {
    e.preventDefault();
  });
  $("#connect")
    .click(function () {
      connect();
    })
    .trigger("click");
  $("#disconnect").click(function () {
    disconnect();
  });
  $("#send").click(function () {
    sendMessage();
    $("*:focus").blur();
  });
  $(`[data-toggle="tooltip"]`).tooltip();
  document.body.addEventListener(
    "pointerdown",
    (event) => {
      $(`[data-toggle="tooltip"]`).tooltip("hide");
    },
    { capture: true }
  );
  observeResize();
  observeMessages();
});
