package net.s13e.app.websocket.services;

import org.springframework.stereotype.Service;

import net.s13e.app.websocket.entities.ChatCommand;
import net.s13e.app.websocket.entities.ChatContext;
import net.s13e.app.websocket.entities.ChatEntity;
import net.s13e.app.websocket.entities.ChatEntityContextBuilder;
import net.s13e.app.websocket.entities.ChatMessage;
import net.s13e.app.websocket.entities.ChatMessageBuilder;
import net.s13e.app.websocket.entities.ChatMessageContextBuilder;
import net.s13e.app.websocket.entities.DeleteTarget;
import net.s13e.app.websocket.entities.DeleteTargetBuilder;
import net.s13e.app.websocket.entities.UserData;
import net.s13e.app.websocket.entities.UserDataBuilder;
import net.s13e.app.websocket.repositories.ChatMessageRepository;
import net.s13e.app.websocket.repositories.UserDataRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

/**
 * ChatContextを返すService
 */
@Service
public class ChatContextService {
    private final ChatMessageRepository chatMessageRepository;
    private final UserDataRepository userDataRepository;
    private final ConcurrentLinkedDeque<ChatMessage> messageQueue;

    public ChatContextService(ChatMessageRepository chatMessageRepository, UserDataRepository userDataRepository) {
        this.chatMessageRepository = chatMessageRepository;
        this.userDataRepository = userDataRepository;
        this.messageQueue = new ConcurrentLinkedDeque<ChatMessage>();
    }

    /**
     * DBから得たrow resultのリストをChatEntity系のリストに変換する
     * 
     * @param <T>     ChatEntityを実装したクラス
     * @param input   DBから得たrow resultのリスト {@code @Nullable}
     * @param builder ChatEntity系のbuilderメソッドの関数オブジェクト {@code @NonNull}
     * @return 変換されたChatEntity系のリスト {@code @NonNull}
     */
    @NonNull
    private <T extends ChatEntity> List<T> buildList(@Nullable List<Map<String, Object>> input,
            @NonNull Function<Map<String, Object>, T> builder) {
        if (input == null || input.size() == 0) {
            return Collections.<T>emptyList();
        }
        return input.stream()
                .map(builder)
                .toList();
    }

    /**
     * UserDataをDBにinsertする
     * 単独のUserDataをSetに詰めてRepositoryに渡す
     * 
     * @param userData UserDataのSet {@code @NonNull}
     */
    private void storeUserData(@NonNull UserData userData) {
        this.userDataRepository.upsert(Set.of(userData));
    }

    /**
     * ChatMessageのリストからUserDataのSetを作成しDBに保存する
     * UserDataは重複を許す意味がないのでSetで重複を排除する
     * ChatMessageはidがULIDなので時系列だがUserDataとしては最新のものを保存したいので逆順にソートしてからSetにする
     * 
     * @param messages ChatMessageのリスト {@code @NonNull}
     */
    private void storeUserData(@NonNull List<ChatMessage> messages) {
        Set<UserData> userDataSet = messages.stream()
                .sorted(Comparator.reverseOrder())
                .map(m -> UserDataBuilder.builder()
                        .sessionId(UUID.fromString(m.sender()))
                        .name(m.name())
                        .build())
                .collect(Collectors.toSet());
        this.userDataRepository.upsert(userDataSet);
    }

    /**
     * ControllerからやってきたChatMessageをキューに詰める
     * ConcurrentLinkedDequeへの単一の操作なので同期は不要
     * 
     * @param message ChatMessage {@code @NonNull}
     */
    public void pushMessage(@NonNull ChatMessage message) {
        this.messageQueue.add(message);
    }

    /**
     * 現時点で生存しているChatMessageのリストを含んだChatContextを返す
     * 1分で消えるチャットなのでページングは未実装
     * 
     * @return ChatMessageのリストを含んだChatContext {@code @Nullable}
     */
    @Nullable
    public ChatContext getContextForList() {
        List<ChatMessage> messages = this.<ChatMessage>buildList(
                this.chatMessageRepository.findAll(),
                ChatMessageBuilder::build);
        if (messages.size() > 0) {
            return ChatMessageContextBuilder.builder()
                    .command(ChatCommand.APPEND)
                    .messages(messages)
                    .build();
        } else {
            return null;
        }
    }

    /**
     * 削除対象のDeleteTargetのリストを含んだChatContextを返す
     * このメソッドはスケジュールで呼ばれる
     * ページングは未実装
     * 
     * @return DeleteTargetのリストを含んだChatContext {@code @Nullable}
     */
    @Nullable
    public ChatContext getContextForCleanUp() {
        List<DeleteTarget> messages = this.<DeleteTarget>buildList(
                this.chatMessageRepository.findOneMinutePassed(),
                DeleteTargetBuilder::build);
        if (messages.size() > 0) {
            this.chatMessageRepository.delete(messages);
            return ChatMessageContextBuilder.builder()
                    .command(ChatCommand.DELETE)
                    .messages(messages)
                    .build();
        } else {
            return null;
        }
    }

    /**
     * クライアントにUserDataを通知するためのChatContextを返す
     * セッションIDを受け取ってDBに既存ユーザ名があったらそれをChatContextに詰めて返す
     * DBに既存ユーザ名がなかったら空文字をChatContextに詰めて返す
     * RuntimeException以外が起こることは考慮されていないが戻り値は{@code @Nullable}
     * 
     * @param uuid セッションIDとしてのUUID {@code @NonNull}
     * @return 接続者のUserDataを含んだChatContext {@code @Nullable}
     */
    @Nullable
    public ChatContext getContextForSetUserData(@NonNull UUID uuid) {
        try {
            Map<String, Object> rowUserData = this.userDataRepository.findById(uuid);
            if (rowUserData == null || rowUserData.size() == 0) {
                UserData userData = UserDataBuilder.builder()
                        .sessionId(uuid)
                        .name("")
                        .build();
                this.storeUserData(userData);
                return ChatEntityContextBuilder.builder()
                        .command(ChatCommand.SET)
                        .entity(userData)
                        .build();
            } else {
                return ChatEntityContextBuilder.builder()
                        .command(ChatCommand.SET)
                        .entity(UserDataBuilder.build(rowUserData))
                        .build();
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Topicに流すChatMessageのリストを含んだChatContextを返す
     * このメソッドはスケジュールで呼ばれる
     * ConcurrentLinkedDequeへの複数操作なので同期する
     * キューが空の場合は {@code @Nullable}
     * キューが空でない場合はキューの内容をディープコピーしてリストに変換し、キューを空にし、DBに保存してからChatContextに詰めて返す
     * ページングは未実装
     * 
     * @return ChatMessageのリストを含んだChatContext {@code @Nullable}
     */
    @Nullable
    public synchronized ChatContext getContextForBroadcastMessages() {
        if (!this.messageQueue.isEmpty()) {
            List<ChatMessage> messages = this.messageQueue.stream()
                    .map((m) -> m.clone())
                    .toList();
            this.messageQueue.clear();
            this.chatMessageRepository.insert(messages);
            this.storeUserData(messages);
            return ChatMessageContextBuilder.builder()
                    .command(ChatCommand.APPEND)
                    .messages(messages)
                    .build();
        } else {
            return null;
        }
    }
}
