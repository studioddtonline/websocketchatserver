package net.s13e.app.websocket.aspects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingAspect {
    private static final Logger log = LogManager.getLogger(LoggingAspect.class);

    private static enum LOG_FORMAT {
        NULL_RETURN("%s: Null return"),
        ZERO_RETURN("%s: Zero return"),
        UNHANDLED_EXCEPTION("%s: Unhandled Exception: %s");

        private String value;

        private LOG_FORMAT(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

    private void errorLogging(LOG_FORMAT format, Object... message) {
        log.error(String.format(format.getValue(), message));
    }

    @AfterReturning(pointcut = "@annotation(net.s13e.app.websocket.annotations.LoggingWhenNullReturns)", returning = "result")
    public void loggingForNullReturn(JoinPoint jp, Object result) {
        if (result == null) {
            this.errorLogging(
                    LOG_FORMAT.NULL_RETURN,
                    jp.getSignature().toShortString());
        }
    }

    @AfterReturning(pointcut = "@annotation(net.s13e.app.websocket.annotations.LoggingWhenZeroReturns)", returning = "result")
    public void loggingForZeroReturn(JoinPoint jp, int result) {
        if (result == 0) {
            this.errorLogging(
                    LOG_FORMAT.ZERO_RETURN,
                    jp.getSignature().toShortString());
        }
    }

    @AfterThrowing(pointcut = "within(net.s13e.websocket.*)", throwing = "e")
    public void loggingForController(JoinPoint jp, Throwable e) {
        this.errorLogging(
                LOG_FORMAT.UNHANDLED_EXCEPTION,
                jp.getSignature().toShortString(),
                e.getClass().getSimpleName());
    }
}
