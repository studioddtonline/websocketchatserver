package net.s13e.app.websocket.repositories;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.s13e.app.websocket.annotations.LoggingWhenNullReturns;
import net.s13e.app.websocket.annotations.LoggingWhenZeroReturns;
import net.s13e.app.websocket.entities.ChatMessage;
import net.s13e.app.websocket.entities.DeleteTarget;

/**
 * chat_messagesテーブルに対するRepository
 */
@Repository
public class ChatMessageRepository {
    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert simpleJdbcInsert;
    private static final String SQL_FIND_FORMAT = "SELECT * FROM chat_messages %s;";
    private static final String SQL_DELETE_FORMAT = "DELETE FROM chat_messages %s;";
    private static final List<Map<String, Object>> EMPTY_RESULT = Collections.<Map<String, Object>>emptyList();

    public ChatMessageRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.simpleJdbcInsert = new SimpleJdbcInsert(this.jdbcTemplate)
                .withTableName("chat_messages");
    }

    /**
     * SQLのWHERE句に相当する文字列を受け取ってSQLを生成し検索結果をrow resultのリストで返す
     * リストがnullまたは空の場合は空のリストを返す
     * DataAccessException系の例外が発生したらnullを返す
     * nullを返す場合はログに出力される
     * 
     * @param whereQuery SQLのWHERE句に相当する文字列
     * @return 検索結果を表すList<Map<String, Object>> {@code @Nullable}
     */
    @Transactional(readOnly = true)
    @LoggingWhenNullReturns
    @Nullable
    public List<Map<String, Object>> find(@NonNull String whereQuery) {
        try {
            List<Map<String, Object>> result = this.jdbcTemplate
                    .queryForList(String.format(SQL_FIND_FORMAT, whereQuery));
            if (result == null || result.size() == 0) {
                return EMPTY_RESULT;
            } else {
                return result;
            }
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (DataAccessException e) {
            return null;
        }
    }

    /**
     * 現在生存しているメッセージを検索してrow resultのリストを返す
     * {@code @Nullable}
     * 
     * @return 検索結果を表すList<Map<String, Object>> {@code @Nullable}
     */
    @Transactional(readOnly = true)
    @Nullable
    public List<Map<String, Object>> findAll() {
        return this.find("");
    }

    /**
     * 1分より前に投稿されたメッセージを検索してrow resultのリストを返す
     * {@code @Nullable}
     * 
     * @return 検索結果を表すList<Map<String, Object>> {@code @Nullable}
     */
    @Transactional(readOnly = true)
    @Nullable
    public List<Map<String, Object>> findOneMinutePassed() {
        final String whereQuery = String.format(" WHERE created_at < '%s'",
                new Timestamp(System.currentTimeMillis() - (60L * 1000L)).toString());
        return this.find(whereQuery);
    }

    /**
     * 渡されたChatMessageのリストからSQLParameterSourceの配列を作りSimpleJdbcInsert経由でバッチアップデートする
     * DataAccessExceptionが発生したらロールバックして0を返す
     * 0が返る場合はログに出力される
     * MySQLはrewriteBatchedStatementsというフラグが有効のときはinsertの戻り値が想定外になるため
     * 雑に入力件数をそのまま返している
     * 
     * @param messages ChatMessageのリスト
     * @return insertに成功した件数を表すint
     */
    @Transactional(rollbackFor = DataAccessException.class)
    @LoggingWhenZeroReturns
    @NonNull
    public int insert(@NonNull List<ChatMessage> messages) {
        try {
            SqlParameterSource[] params = messages.stream()
                    .map(BeanPropertySqlParameterSource::new)
                    .toArray(BeanPropertySqlParameterSource[]::new);
            this.simpleJdbcInsert.executeBatch(params);
            return messages.size();
        } catch (DataAccessException e) {
            return 0;
        }
    }

    /**
     * 渡されたDeleteTargetのリストからSQLを作り、対象を削除するアップデートをする
     * DataAccessExceptionが発生したらロールバックして0を返す
     * 0が返る場合はログに出力される
     * おそらくrewriteBatchedStatementsフラグの影響を受けているので成功時は固定で入力件数を返す
     * 
     * @param messages DeleteTargetのリスト
     * @return 削除に成功した件数を表すint
     */
    @Transactional(rollbackFor = DataAccessException.class)
    @LoggingWhenZeroReturns
    @NonNull
    public int delete(@NonNull List<DeleteTarget> messages) {
        try {
            final String whereQuery = messages.stream()
                    .map(m -> "'" + m.id() + "'")
                    .collect(Collectors.joining(", ", " WHERE `id` IN(", ")"));
            this.jdbcTemplate.update(String.format(SQL_DELETE_FORMAT, whereQuery));
            return messages.size();
        } catch (EmptyResultDataAccessException e) {
            return 0;
        } catch (DataAccessException e) {
            return 0;
        }
    }
}
