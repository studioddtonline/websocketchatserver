package net.s13e.app.websocket.repositories;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import net.s13e.app.websocket.annotations.LoggingWhenNullReturns;
import net.s13e.app.websocket.annotations.LoggingWhenZeroReturns;
import net.s13e.app.websocket.entities.UserData;

/**
 * chat_usersテーブルに対するRepository
 */
@Repository
public class UserDataRepository {
    private final JdbcTemplate jdbcTemplate;
    private static final String FIND_BY_ID_FORMAT = "SELECT * FROM chat_users WHERE `session_id` = '%s';";
    private static final String UPSERT_FORMAT = """
            INSERT INTO chat_users (`session_id`, `name`)
            VALUES ('%s', '%s')
            ON DUPLICATE KEY UPDATE `name` = VALUES(`name`);
            """;
    private static final String DELETE_BY_ID_FORMAT = "DELETE FROM chat_users WHERE `session_id` = '%s';";
    private static final Map<String, Object> EMPTY_RESULT = Collections.<String, Object>emptyMap();

    public UserDataRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /**
     * 渡されたUserDataのSetからSQLの配列を作りJDBC経由でバッチアップデートする
     * DataAccessExceptionが発生したらロールバックして0を返す
     * 0が返る場合はログに出力される
     * MySQLはrewriteBatchedStatementsというフラグが有効のときはinsertの戻り値が想定外になるため
     * 雑に入力件数をそのまま返している
     * 
     * @param userDataSet UserDataのSet
     * @return 挿入した件数を表すint
     */
    @Transactional(rollbackFor = DataAccessException.class)
    @LoggingWhenZeroReturns
    @NonNull
    public int upsert(@NonNull Set<UserData> userDataSet) {
        try {
            String[] batchSql = userDataSet.stream()
                    .map(u -> String.format(
                            UPSERT_FORMAT,
                            u.sessionId().toString(),
                            u.name()))
                    .toArray(String[]::new);
            this.jdbcTemplate.batchUpdate(batchSql);
            return userDataSet.size();
        } catch (DataAccessException e) {
            return 0;
        }
    }

    /**
     * 渡されたUserDataからSQLを作り、対象を削除するアップデートをする
     * このメソッドは使用されていない
     * DataAccessExceptionが発生したらロールバックして0を返す
     * 0が返る場合はログに出力される
     * おそらくrewriteBatchedStatementsフラグの影響を受けているので成功時は固定で1を返す
     * 
     * @param userData 削除対象のUserData
     * @return 削除された件数を表すint
     */
    @Transactional(rollbackFor = DataAccessException.class)
    @LoggingWhenZeroReturns
    @NonNull
    public int delete(@NonNull UserData userData) {
        try {
            this.jdbcTemplate.update(
                    String.format(
                            DELETE_BY_ID_FORMAT,
                            userData.sessionId().toString()));
            return 1;
        } catch (DataAccessException e) {
            return 0;
        }
    }

    /**
     * 渡されたUUIDからSQLを作り検索結果を1件返す
     * 結果が0またはnullだった場合は空のMapを返す
     * queryForMapは対象のUUIDがDBにない時はEmptyResultDataAccessExceptionを吐くが、その場合は空のMapを返す
     * DataAccessExceptionが発生したらnullを返す
     * 
     * @param uuid 検索対象のUUID
     * @return JDBCTemplateからのrow resultであるMap<String, Object>
     */
    @Transactional(readOnly = true)
    @LoggingWhenNullReturns
    @Nullable
    public Map<String, Object> findById(@NonNull UUID uuid) {
        try {
            Map<String, Object> result = this.jdbcTemplate.queryForMap(
                    String.format(
                            FIND_BY_ID_FORMAT,
                            uuid.toString()));
            if (result == null || result.size() == 0) {
                return EMPTY_RESULT;
            } else {
                return result;
            }
        } catch (EmptyResultDataAccessException e) {
            return EMPTY_RESULT;
        } catch (DataAccessException e) {
            return null;
        }
    }
}
