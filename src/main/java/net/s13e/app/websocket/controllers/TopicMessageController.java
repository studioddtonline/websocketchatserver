package net.s13e.app.websocket.controllers;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.lang.NonNull;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import io.azam.ulidj.ULID;
import net.s13e.app.websocket.entities.ChatContext;
import net.s13e.app.websocket.entities.ChatMessage;
import net.s13e.app.websocket.entities.ChatMessageBuilder;
import net.s13e.app.websocket.services.ChatContextService;

@Controller
public class TopicMessageController {
    private final SimpMessagingTemplate brokerMessagingTemplate;
    private final ChatContextService service;

    public TopicMessageController(ChatContextService service,
            SimpMessagingTemplate brokerMessagingTemplate) {
        this.service = service;
        this.brokerMessagingTemplate = brokerMessagingTemplate;
    }

    @MessageMapping("/message")
    public void messaging(@NonNull @Payload ChatMessage message) {
        ChatMessage escapedMessage = ChatMessageBuilder.builder()
                .name(HtmlUtils.htmlEscape(message.name()))
                .content(HtmlUtils.htmlEscape(message.content()))
                .id(ULID.random())
                .sender(message.sender())
                .createdAt(new Timestamp(System.currentTimeMillis()))
                .build();
        this.service.pushMessage(escapedMessage);
    }

    @Scheduled(initialDelay = 1000, fixedRate = 1000)
    public void bradcastMessages() {
        ChatContext context = this.service.getContextForBroadcastMessages();
        if (context != null) {
            this.brokerMessagingTemplate.convertAndSend("/topic/message", List.<ChatContext>of(context));
        }
    }

    @Scheduled(initialDelay = 10000, fixedRate = 10000)
    public void cleaning() {
        ChatContext context = this.service.getContextForCleanUp();
        if (context != null) {
            this.brokerMessagingTemplate.convertAndSend("/topic/clean", List.<ChatContext>of(context));
        }
    }
}
