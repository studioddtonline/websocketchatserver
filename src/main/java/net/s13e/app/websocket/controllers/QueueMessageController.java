package net.s13e.app.websocket.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Stream;

import org.springframework.lang.NonNull;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import net.s13e.app.websocket.entities.ChatCommand;
import net.s13e.app.websocket.entities.ChatContext;
import net.s13e.app.websocket.entities.ChatEntity;
import net.s13e.app.websocket.entities.ChatMessageContextBuilder;
import net.s13e.app.websocket.services.ChatContextService;

@Controller
public class QueueMessageController {
    private final ChatContextService service;
    private static final ChatContext CLEAR_CHAT_CONTEXT = ChatMessageContextBuilder.builder()
            .command(ChatCommand.CLEAR)
            .messages(Collections.<ChatEntity>emptyList())
            .build();

    public QueueMessageController(ChatContextService service) {
        this.service = service;
    }

    @MessageMapping("/init")
    @SendToUser("/queue/init")
    @NonNull
    public List<ChatContext> initializeClient(@NonNull SimpMessageHeaderAccessor headerAccessor) {
        UUID sessionId = UUID.fromString((String) headerAccessor.getSessionAttributes().get("sessionId"));
        return Stream.<ChatContext>of(
                this.service.getContextForSetUserData(sessionId),
                CLEAR_CHAT_CONTEXT,
                this.service.getContextForList())
                .filter(Objects::nonNull)
                .toList();
    }
}
