package net.s13e.app.websocket.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.jdbc.config.annotation.web.http.EnableJdbcHttpSession;

/**
 * Spring SessionのJava Config
 * JDBC経由でDBにセッション情報を記録する
 * DB上のInactiveInterval設定はEnableJdbcHttpSessionアノテーションのmaxInactiveIntervalInSecondsプロパティで行う
 * propertiesのspring.session.timeoutやserver.servlet.session.timeoutに設定された値はDBには反映されないので注意
 */
@Configuration
@EnableJdbcHttpSession(maxInactiveIntervalInSeconds = 86400)
public class HttpSessionConfig {
}
