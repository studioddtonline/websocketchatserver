package net.s13e.app.websocket.configs;

import java.util.Map;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketTransportRegistration;
import org.springframework.web.socket.server.HandshakeInterceptor;

import jakarta.servlet.http.HttpSession;

import org.springframework.session.Session;
import org.springframework.session.web.socket.config.annotation.AbstractSessionWebSocketMessageBrokerConfigurer;

/**
 * WebSocket関連のJava Config
 * WebSocket通信発生時にSession情報を更新してもらうためにAbstractSessionWebSocketManagerBrokerConfigurer<Session>を継承している
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractSessionWebSocketMessageBrokerConfigurer<Session> {

    /**
     * WebSocketトランスポート層の設定値の指定をする
     * WebSocketトランスポートメッセージの最大値の設定
     * WebSocketトランスポート送信バッファサイズの設定
     * intの最大値に設定しているが事実上の限界値はMySQLパケットの最大値付近になると思われる
     * 
     * @param registration
     */
    @Override
    public void configureWebSocketTransport(WebSocketTransportRegistration registration) {
        registration
                .setMessageSizeLimit(Integer.MAX_VALUE)
                .setSendBufferSizeLimit(Integer.MAX_VALUE);
    }

    /**
     * STOMPエンドポイント設定をする
     * STOMPエンドポイントの設定
     * STOMPエンドポイントのアクセスコントロール設定
     * HTTPSessionのセッションIDをsession attributeに保存するためのInterseptorの登録
     * 
     * @param registry
     */
    @Override
    public void configureStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/stompOverWebsocket")
                .setAllowedOrigins("*")
                .addInterceptors(new HandshakeInterceptor() {

                    @Override
                    public boolean beforeHandshake(
                            ServerHttpRequest request,
                            ServerHttpResponse response,
                            WebSocketHandler wsHandler,
                            final Map<String, Object> attributes) throws Exception {
                        if (request instanceof ServletServerHttpRequest) {
                            ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
                            HttpSession session = servletRequest.getServletRequest().getSession();
                            attributes.put("sessionId", session.getId());
                        }
                        return true;
                    }

                    @Override
                    public void afterHandshake(
                            ServerHttpRequest request,
                            ServerHttpResponse response,
                            WebSocketHandler wsHandler,
                            Exception exception) {
                    }
                });
    }

    /**
     * メッセージブローカーのハートビートで用いるタスクスケジューラのBean登録
     * Bean名を変更するとSpringから警告を受けるため、taskSchedulerというBean名を変えてはいけない
     * 
     * @return TaskScheduler
     */
    @Bean
    public TaskScheduler taskScheduler() {
        return new ThreadPoolTaskScheduler();
    }

    /**
     * メッセージブローカーの設定をする
     * パフォーマンスに影響のある設定値を無効に設定
     * メッセージブローカーのパスプレフィックスの設定
     * SimpleBrokerを有効にしてWebSocketエンドポイントのプレフィックスを設定
     * ハートビートの設定
     * 
     * @param config
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.setPreservePublishOrder(false)
                .setApplicationDestinationPrefixes("/app")
                .setUserDestinationPrefix("/user")
                .enableSimpleBroker("/topic", "/queue")
                .setTaskScheduler(taskScheduler())
                .setHeartbeatValue(new long[] { 30 * 1000, 30 * 1000 });
    }
}
