package net.s13e.app.websocket.configs;

import java.io.IOException;

import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.EncodedResourceResolver;
import org.springframework.web.servlet.resource.PathResourceResolver;

/**
 * Spring MVC関連のJava Config
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
        private static final String[] CLASSPASS_WEBJARS_LOCATIONS = {
                        "classpath:/META-INF/resources/webjars/",
                        "/webjars/"
        };

        private static final String[] CORS_ALLOWED_METHODS = {
                        "GET",
                        "HEAD",
                        "OPTIONS"
        };

        public static final String[] CORS_ALLOWED_HEADERS = {
                        "Upgrade",
                        "Connection",
                        "Sec-WebSocket-Extensions",
                        "Sec-WebSocket-Key",
                        "Sec-WebSocket-Protocol",
                        "Sec-WebSocket-Version"
        };

        
        /** 
         * リソースハンドラで参照するBean登録
         * staticリソースのパスを得るために使用している
         * @return Resources
         */
        @Bean
        public WebProperties.Resources resources() {
                return new WebProperties.Resources();
        }

        
        /** 
         * リソースハンドラの登録
         * WebJarsリソースへの設定と一般リソースへの設定をする
         * 開発時はresourceChainのchacheResourcesをfalseにしているが運用時はtrueにするほうが良い
         * WebJarsに対しては圧縮されたリソースへのアクセスを提供する
         * 一般リソースについてはSPAアプリケーションに対応するように設定する
         * 
         * @param registry
         */
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
                // WebJarsリソースへの設定
                registry.addResourceHandler("/webjars/**")
                                .addResourceLocations(CLASSPASS_WEBJARS_LOCATIONS)
                                .resourceChain(false)
                                .addResolver(new EncodedResourceResolver());
                /**
                 * 一般リソースへの設定
                 * SPAアプリケーションではマッピングのないパスへのアクセスがあり得るが、
                 * その場合に一意的にindex.htmlを返すリゾルバを作成している
                 */
                registry.addResourceHandler("/**")
                                .addResourceLocations(resources().getStaticLocations())
                                .resourceChain(false)
                                .addResolver(new PathResourceResolver() {
                                        @Override
                                        protected Resource getResource(String resourcePath, Resource location)
                                                        throws IOException {
                                                Resource resource = super.getResource(resourcePath, location);
                                                return resource != null ? resource
                                                                : super.getResource("index.html", location);
                                        }
                                });
        }

        
        /** 
         * CORS設定
         * 別オリジンからWebSocket接続を許容するように設定している
         * @param registry
         */
        @Override
        public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                                .allowedOrigins("*")
                                .allowedMethods(CORS_ALLOWED_METHODS)
                                .allowedHeaders(CORS_ALLOWED_HEADERS);
        }

        
        /** 
         * デフォルトサーブレットハンドリングの設定
         * staticリソースへのアクセス解決をデフォルトサーブレットに転送できるように指定
         * @param configurer
         */
        @Override
        public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
                configurer.enable("/");
        }
}