package net.s13e.app.websocket.entities;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public record UserData(UUID sessionId, String name) implements ChatEntity, Serializable {

    @NonNull
    public boolean equals(@NonNull UserData userData) {
        return this.sessionId().equals(userData.sessionId());
    }

    @Override
    @NonNull
    public boolean equals(@Nullable Object o) {
        if (o instanceof UserData) {
            return this.equals((UserData) o);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.sessionId().hashCode();
    }
}
