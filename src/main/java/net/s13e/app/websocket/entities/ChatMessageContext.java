package net.s13e.app.websocket.entities;

import java.util.List;

import org.springframework.lang.NonNull;

public record ChatMessageContext(
        @NonNull ChatCommand command,
        @NonNull List<? extends ChatEntity> messages)
        implements ChatContext {
}
