package net.s13e.app.websocket.entities;

import java.util.List;

import org.springframework.lang.NonNull;

public class ChatMessageContextBuilder {
    private ChatCommand command;
    private List<? extends ChatEntity> messages;

    private ChatMessageContextBuilder() {
    }

    @NonNull
    public static ChatMessageContextBuilder builder() {
        return new ChatMessageContextBuilder();
    }

    @NonNull
    public ChatMessageContext build() throws RuntimeException {
        if (this.command == null || this.messages == null) {
            throw new RuntimeException();
        }
        return new ChatMessageContext(this.command, this.messages);
    }

    @NonNull
    public ChatMessageContextBuilder command(@NonNull ChatCommand command) {
        this.command = command;
        return this;
    }

    @NonNull
    public ChatMessageContextBuilder messages(@NonNull List<? extends ChatEntity> messages) {
        this.messages = messages;
        return this;
    }
}
