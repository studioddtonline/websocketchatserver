package net.s13e.app.websocket.entities;

import org.springframework.lang.NonNull;

public class ChatEntityContextBuilder {
    private ChatCommand command;
    private ChatEntity entity;

    private ChatEntityContextBuilder() {
    }

    @NonNull
    public static ChatEntityContextBuilder builder() {
        return new ChatEntityContextBuilder();
    }

    @NonNull
    public ChatEntityContext build() throws RuntimeException {
        if (this.command == null || this.entity == null) {
            throw new RuntimeException();
        }
        return new ChatEntityContext(this.command, this.entity);
    }

    @NonNull
    public ChatEntityContextBuilder command(@NonNull ChatCommand command) {
        this.command = command;
        return this;
    }

    @NonNull
    public ChatEntityContextBuilder entity(@NonNull ChatEntity entity) {
        this.entity = entity;
        return this;
    }

}
