package net.s13e.app.websocket.entities;

import java.util.Map;

import org.springframework.lang.NonNull;

import io.azam.ulidj.ULID;

public class DeleteTargetBuilder {
    private String id;

    private DeleteTargetBuilder() {
    }

    @NonNull
    public static DeleteTargetBuilder builder() {
        return new DeleteTargetBuilder();
    }

    @NonNull
    public static DeleteTarget build(@NonNull Map<String, Object> input) throws RuntimeException {
        return DeleteTargetBuilder.builder()
                .id((String) input.get("id"))
                .build();
    }

    @NonNull
    public DeleteTarget build() throws RuntimeException {
        if (this.id == null) {
            throw new RuntimeException();
        }
        return new DeleteTarget(this.id);
    }

    @NonNull
    public DeleteTargetBuilder id(@NonNull String id) {
        if (!ULID.isValid(id)) {
            throw new IllegalArgumentException();
        }
        this.id = id;
        return this;
    }
}
