package net.s13e.app.websocket.entities;

import java.io.Serializable;

import org.springframework.lang.NonNull;

public record DeleteTarget(@NonNull String id) implements ChatEntity, Serializable {

}
