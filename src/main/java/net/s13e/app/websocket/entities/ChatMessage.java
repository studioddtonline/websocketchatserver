package net.s13e.app.websocket.entities;

import java.io.Serializable;
import java.sql.Timestamp;

import org.springframework.lang.NonNull;

public record ChatMessage(
        @NonNull String name,
        @NonNull String content,
        @NonNull String id,
        @NonNull String sender,
        @NonNull Timestamp createdAt)
        implements ChatEntity, Serializable, Cloneable, Comparable<ChatMessage> {
    @Override
    @NonNull
    public ChatMessage clone() {
        return new ChatMessage(name, content, id, sender, (Timestamp) createdAt.clone());
    }

    @Override
    public int compareTo(ChatMessage m) {
        try {
            return this.id().compareTo(m.id());
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ChatMessage) {
            return this.id() == ((ChatMessage) o).id();
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.id().hashCode();
    }
}
