package net.s13e.app.websocket.entities;

import java.util.Map;
import java.util.UUID;

import org.springframework.lang.NonNull;

public class UserDataBuilder {
    private UUID sessionId;
    private String name;

    private UserDataBuilder() {
    }

    @NonNull
    public static UserDataBuilder builder() {
        return new UserDataBuilder();
    }

    @NonNull
    public static UserData build(@NonNull Map<String, Object> row) throws RuntimeException {
        return UserDataBuilder.builder()
                .sessionId(UUID.fromString((String) row.get("session_id")))
                .name((String) row.get("name"))
                .build();
    }

    @NonNull
    public UserData build() throws RuntimeException {
        if (this.sessionId == null || this.name == null) {
            throw new RuntimeException();
        }
        return new UserData(this.sessionId, this.name);
    }

    @NonNull
    public UserDataBuilder sessionId(@NonNull UUID sessionId) {
        this.sessionId = sessionId;
        return this;
    }

    @NonNull
    public UserDataBuilder name(@NonNull String name) {
        this.name = name;
        return this;
    }
}
