package net.s13e.app.websocket.entities;

import org.springframework.lang.NonNull;

public record ChatEntityContext(
        @NonNull ChatCommand command,
        @NonNull ChatEntity entity)
        implements ChatContext {
}
