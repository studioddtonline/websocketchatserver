package net.s13e.app.websocket.entities;

public enum ChatCommand {
    APPEND, CLEAR, DELETE, SET, INFO, ERROR;
}
