package net.s13e.app.websocket.entities;

import java.sql.Timestamp;
import java.util.Map;
import java.util.UUID;

import org.springframework.lang.NonNull;

import io.azam.ulidj.ULID;

public class ChatMessageBuilder {
    private String name;
    private String content;
    private String id;
    private Timestamp createdAt;
    private String sender;

    private ChatMessageBuilder() {
    }

    @NonNull
    public static ChatMessageBuilder builder() {
        return new ChatMessageBuilder();
    }

    @NonNull
    public static ChatMessage build(@NonNull Map<String, Object> row) throws RuntimeException {
        return ChatMessageBuilder.builder()
                .name((String) row.get("name"))
                .content((String) row.get("content"))
                .id((String) row.get("id"))
                .sender((String) row.get("sender"))
                .createdAt((Timestamp) row.get("created_at"))
                .build();
    }

    @NonNull
    public ChatMessage build() throws RuntimeException {
        if (this.name == null
                || this.content == null
                || this.id == null
                || this.sender == null
                || this.createdAt == null) {
            throw new RuntimeException();
        }
        return new ChatMessage(this.name, this.content, this.id, this.sender, this.createdAt);
    }

    @NonNull
    public ChatMessageBuilder id(@NonNull String id) {
        if (!ULID.isValid(id)) {
            throw new IllegalArgumentException();
        }
        this.id = id;
        return this;
    }

    @NonNull
    public ChatMessageBuilder name(@NonNull String name) {
        this.name = name;
        return this;
    }

    @NonNull
    public ChatMessageBuilder content(@NonNull String content) {
        this.content = content;
        return this;
    }

    @NonNull
    public ChatMessageBuilder sender(@NonNull String sender) {
        if (!(UUID.fromString(sender) instanceof UUID)) {
            throw new IllegalArgumentException();
        }
        this.sender = sender;
        return this;
    }

    @NonNull
    public ChatMessageBuilder createdAt(@NonNull Timestamp createdAt) {
        this.createdAt = createdAt;
        return this;
    }
}
